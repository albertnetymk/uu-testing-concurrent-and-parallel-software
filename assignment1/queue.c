// #include "queue.h"

#define SIZE 100000                   // The fixed number of slots in the buffer.
volatile int queue[SIZE], head, tail; // The buffer itself, together with a head and tail pointer.

#define use_lock

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
// Initialization
void initialize(void) {
  head = tail = 0;
}

// The enqueue operation
int enqueue(int value) {
#ifdef use_lock
  pthread_mutex_lock(&mutex);
  if (((head - tail) % SIZE == 0) && (head != tail)) { // If there are already SIZE elements,
    pthread_mutex_unlock(&mutex);
    return 0;                                        //  the queue is full.
  }
  int i = tail++ % SIZE;                             // Otherwise, advance the tail,
  queue[i] = value;                                  //  and insert the new element.
  pthread_mutex_unlock(&mutex);
  return 1;
#else
  if (((head - tail) % SIZE == 0) && (head != tail)) { // If there are already SIZE elements,
    return 0;                                        //  the queue is full.
  }
  int i = tail++ % SIZE;                             // Otherwise, advance the tail,
  queue[i] = value;                                  //  and insert the new element.
  return 1;
#endif
}

int dequeue(int *out) {
#ifdef use_lock
  pthread_mutex_lock(&mutex);
  if (head == tail) {      // If head == tail the queue is empty
    pthread_mutex_unlock(&mutex);
    return 0;            //  and there is nothing to dequeue.
  }
  int i = head++ % SIZE; // Otherwise advance the head
  *out = queue[i];       //  and retrieve the value of the element there.
  pthread_mutex_unlock(&mutex);
  return 1;
#else
  if (head == tail) {      // If head == tail the queue is empty
    return 0;            //  and there is nothing to dequeue.
  }
  int i = head++ % SIZE; // Otherwise advance the head
  *out = queue[i];       //  and retrieve the value of the element there.
  return 1;
#endif
}
