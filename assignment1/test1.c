#include <assert.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "queue.c"

#define ENTRIES 1000000

int threads = 1;
int entries[ENTRIES];

double now() {
  struct timeval tv;
  gettimeofday(&tv, 0);
  return tv.tv_sec + tv.tv_usec / 1000000.0;
}

void * enqueuer(void *xa) {
  long n = (long) xa;
  int b = ENTRIES/threads;
  int entry, res;
  for (int i = 0; i < b; i++) {
    entry = b*n + i;
    entries[entry] = enqueue(entry);
  }
  return NULL;
}

void test_enqueuer()
{
  pthread_t *tha;
  void *value;
  long i;
  double t1, t0;

  // Allocate threads handles for pthread_join() below.
  tha = malloc(sizeof(pthread_t) * threads);

  // Fill out the entries array
  for(i = 0; i < ENTRIES; i++)
    entries[i] = 0;

  initialize();

  t0 = now();
  // Create enqueuer threads
  for(i = 0; i < threads; i++) {
    assert(pthread_create(&tha[i], NULL, enqueuer, (void *) i) == 0);
  }
  // Wait until they are all done.
  for(i = 0; i < threads; i++) {
    assert(pthread_join(tha[i], &value) == 0);
  }
  t1 = now();

  // printf("completion time = %f\n", t1-t0);

  // Verify enqueued
  int count = 0;
  for(i = 0; i < ENTRIES; i++)
    if (entries[i]) count++;
  assert(count == SIZE);
  // printf("%d enqueued\n", count);
}

static void *_freq;

void *dequeuer(void *_id)
{
  unsigned int (*freq)[SIZE];
  int id = (int)_id;
  freq = _freq;
  for (int i = 0; i < SIZE/threads; ++i) {
    int x;
    if (dequeue(&x)) {
      assert(x < SIZE);
      freq[id][x]++;
    } else {
      break;
    }
  }
  return NULL;
}

void test_dequeuer()
{
  static unsigned int (*freq)[SIZE];
  static pthread_t *tha;
  tha = malloc(sizeof(pthread_t) * threads);
  _freq = calloc(threads, SIZE*sizeof(int));
  freq = _freq;
  for (int i = 0; i < SIZE; ++i) {
    enqueue(i);
  }

  for (int i = 0; i < threads; ++i) {
    int ret = pthread_create(&tha[i], NULL, &dequeuer, (void*) (size_t)i);
    assert(ret == 0);
  }

  for (int i = 0; i < threads; ++i) {
    int ret = pthread_join(tha[i], NULL);
    assert(ret == 0);
  }

  for (int i = 0; i < SIZE; ++i) {
    int sum = 0;
    for (int j = 0; j < threads; ++j) {
      sum += freq[j][i];
    }
    if (sum > 1) {
      printf("%d is poped %d times\n", i, sum);
    }
    assert(sum <= 1);
  }
}

static int *ted_en;

void *ted_enqueuer(void *_null)
{
  for (int i = 0; i < SIZE; ++i) {
    assert(ted_en[i] == 0);
    ted_en[i] = enqueue(i);
  }

  return NULL;
}

static int *ted_de;
void *ted_dequeuer(void *_null)
{
  for (int i = 0; i < SIZE; ++i) {
    int x;
    int ret = dequeue(&x);
    if (ret) {
      assert(ted_de[x] == 0);
      ted_de[x] = ret;
    }
  }

  return NULL;
}

void test_enqueuer_dequeuer()
{
  static pthread_t tha[2];

  ted_en = calloc(SIZE, sizeof(int));
  ted_de = calloc(SIZE, sizeof(int));

  threads = 2;
  {
    int ret = pthread_create(&tha[0], NULL, &ted_enqueuer, NULL);
    assert(ret == 0);
  }

  {
    int ret = pthread_create(&tha[1], NULL, &ted_dequeuer, NULL);
    assert(ret == 0);
  }

  for (int i = 0; i < threads; ++i) {
    int ret = pthread_join(tha[i], NULL);
    assert(ret == 0);
  }

  int sum = 0;
  int x;

  while (dequeue(&x)) {
    sum++;
  }

  for (int i = 0; i < SIZE; ++i) {
    sum += ted_en[i] + ted_de[i];
  }

  assert(sum == 2*SIZE);
}

void *myenqueuer(void *_id)
{
  int id = (int)_id;
  for (int i = 0; i < SIZE/threads; ++i) {
    entries[id * SIZE/threads + i] = enqueue(id * SIZE/threads + i);
  }
  return NULL;
}

void test_multiple_enqueuer_dequeuer()
{
  memset(entries, 0, sizeof(int) * ENTRIES);

  static unsigned int (*freq)[SIZE];
  static pthread_t *t_enqueuer;
  static pthread_t *t_dequeuer;
  t_enqueuer = malloc(sizeof(pthread_t) * threads);
  t_dequeuer = malloc(sizeof(pthread_t) * threads);
  _freq = calloc(threads, SIZE*sizeof(int));
  freq = _freq;

  for (int i = 0; i < threads; ++i) {
    int ret = pthread_create(&t_enqueuer[i], NULL, &myenqueuer, (void*) (size_t)i);
    assert(ret == 0);
  }

  for (int i = 0; i < threads; ++i) {
    int ret = pthread_create(&t_dequeuer[i], NULL, &dequeuer, (void*) (size_t)i);
    assert(ret == 0);
  }

  for (int i = 0; i < threads; ++i) {
    int ret;
    ret = pthread_join(t_enqueuer[i], NULL);
    assert(ret == 0);
    ret = pthread_join(t_dequeuer[i], NULL);
    assert(ret == 0);
  }

  int sum = 0;
  int x;

  while (dequeue(&x)) {
    sum++;
  }

  for(int i = 0; i < ENTRIES; i++) {
    if (entries[i]) sum++;
  }

  for (int i = 0; i < SIZE; ++i) {
    for (int j = 0; j < threads; ++j) {
      sum += freq[j][i];
    }
  }

  assert(sum == 2*SIZE);
}

int main(int argc, char *argv[]) {

  if (argc < 2) {
    fprintf(stderr, "%s: %s threads\n", argv[0], argv[0]);
    exit(-1);
  }

  int init_threads = threads = atoi(argv[1]);

  if (ENTRIES % threads) {
    fprintf(stderr, "%s: threads argument should be a divisor of %d\n",
        argv[0], ENTRIES);
    exit(-1);
  }

  test_enqueuer();

  head = tail = 0;
  threads = init_threads;
  test_dequeuer();

  head = tail = 0;
  threads = init_threads;
  test_enqueuer_dequeuer();

  head = tail = 0;
  threads = init_threads;
  test_multiple_enqueuer_dequeuer();
  return 0;
}
