1.a the read of `tail` in `if` condition vs `tail++` and `tail++` from two threads

1.b the read of `head` in `if` condition vs `head++` and `head++` from two threads

2.a `main` spawns `N` threads, each of which run `enqueue`, and records the returned value in `entries`. `main` waits until all children threads terminate, and count the number of successful enqueues.

2.b The queue is bounded by its max size, 100K.

2.c Due to lack of proper synchronization, threads may overwrite an occupied slot, and return `1`.

3.

Change `#incude "queue.h"` to `#include "queue.c"` so that there's only one translation unit. For thread sanitizer, `clang -g -fsanitize=thread -pthread test1.c && ./a.out 2` reports data race on `tail`. The read of `tail` on `queue.c` is racing with write of `tail` on `queue.c`.

For helgrind, `clang -g test1.c -pthread && valgrind --tool=helgrind --read-var-info=yes ./a.out 2` reports there's possible data race in `enqueue`, read from `tail` in `if` and write to `tail`.

4.a `dequeuer` records the element frequency in a global array, with each thread writing to its own row.

4.b `test_dequeuer` spawns certain number of threads and joins them. Then, it counts the frequency of each element. Since [0..SIZE-1] is inserted, the frequency of each element should be at most one.

4.c The assertion `sum <= 1` fails if turned on.

4.d Using thread sanitizer, it reports there's data race on `head` in `queue.c`.

5.

`test_enqueuer_dequeuer` creates one enqueuer and one dequeuer. Successful enqueue and dequeue is recorded. Since both threads do the scanning from 0 to SIZE, we expect the final sum to be 2 * SIZE (including the left elements in the queue).

`test_multiple_enqueuer_dequeuer` only differs by the number of enqueuers and dequeuers created.

I use assertion for expected result. Observed assertion failures for each of newly added test.

Thread sanitizer reports data race on `head`, `tail`, and `queue`, since newly added tests exercise both `enqueue` and `dequeue`.

6.

Fixed by using pthread mutex for both `enqueue` and `dequeue`, controlled by `use_lock`. Both `clang -g -fsanitize=thread test1.c -pthread && ./a.out 4` and `valgrind --tool=helgrind ./a.out 4` report no data race.
