## A. FIFO queue using circular ring buffer

```c
enqueue(v) {
  i = FA(&tail);
  while (i+1 == head) ;
  i = i % N;
  elements[i] = v;
}

dequeue() {
  while (true) {
    while (head == tail) ;
    i = head % N
    v = empty
    v = SWAP(&elements[i], empty);
    if (v) {
      head++;
      break;
    }
  }
  return v;
}
```

Some comments about the design of this algorithm:
* empty queue, `head == tail`; full queue, `tail + 1 == head`
* initial state, `head == tail == 0`
* FA, SWAP are two constructs from the assignment instruction file
* `empty` is the special value to indicate empty queue slot

## B proof of validity of FIFO queue

Properties 1 and 3 are proved using linearization point. There are two linearization point candidate in `enqueue`, one for `FA`, the other for
`elements[i] = v;`. Therefore, each is recorded in `history` for the enqueued value. For `dequeue`, the extracted value is recorded with its negated
version. When all enqueuers and dequeuers finish, `history` array is queried for each dequeued pair of values for FIFO order.

Properties 2 is checked by counting the frequency of each dequeued value.

Property 4 is proved because all threads can reach terminate state.

In `queue_crash.pml`, one of the dequeuer exits after extracting the value without advancing `head`, as a simulation of crash. `spin -run -f -l
queue_crash.pml` reports, there's `non-progress cycle`, and `spin -t queue_crash.pml` shows that dequeuers are stuck in the `while` loop. Therefore,
the implementation is not lock-free.
