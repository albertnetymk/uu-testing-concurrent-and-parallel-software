byte head, tail

#define n_actors 3
#define N 8
#define empty -1

int elements[N] = empty

#define history_len (2 * N + N)
int history[history_len] = empty
int history_index

#define FA(v, x) atomic {v = x; x++}
#define SWAP(a, b, c) atomic {a = b; b = c}

#define commit(v) history[history_index] = v -> history_index++

byte finished
active [n_actors] proctype enqueuer() {
  byte i
  int value

  /* starts with 2 to avoid -0 and -1 on dequeue */
  value = 2 + _pid

  d_step {
    FA(i, tail)
    commit(value)
  }

  /* not full */
  i + 1 != head

  i = i % N

  d_step {
    elements[i] = value
    commit(value)
    finished++
  }
}

active [n_actors] proctype dequeuer() {
  byte i
  byte tmp_index
  int v

start:
  i = head
  /* check empty */
  if
  :: i == tail -> goto start
  :: else
  fi

  i = i % N

  v = empty
  SWAP(v, elements[i], empty) ->
  if
  :: v == empty -> goto start
  :: else -> head++
  fi

  d_step {
    commit(-v)
    finished++
  }
}

active proctype linear_checker() {
  finished == 2 * n_actors

  d_step {
    int q_in[history_len]
    int q_out[history_len]
    byte in_index, out_index
    byte i, j
    int v
    for (i in history) {
      if
      :: history[i] < 0 && history[i] != empty ->
           q_out[out_index] = history[i]; out_index++
      :: history[i] > 0 -> q_in[in_index] = history[i]; in_index++
      :: history[i] == empty -> skip
      :: else -> assert(false)
      fi
    }

    /* enqueue should respect dequeue for each pair */
    for (j : 0 .. out_index - 2)  {
      byte cur = j
      for (i in history) {
        if
        :: cur < j+2 && history[i] == q_out[cur] -> cur++
        :: else -> skip
        fi
      }
      assert(cur == j+2);
    }

    /* each element is only dequeued once */
    byte frequent[n_actors]
    for (i in q_out) {
      if
      :: q_out[i] < 0 -> frequent[-q_out[i]-2]++
      :: else
      fi
    }
    for (i in frequent) {
      assert(frequent[i] == 1)
    }
  }
}

init {
  /* at least two slot for the array, for we use one extra slot to distinguish
   * empty from full */
  assert(N > 1)
}
