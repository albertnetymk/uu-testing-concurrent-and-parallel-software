byte head, tail

#define n_actors 2
#define N 8
#define empty -1

int elements[N] = empty

#define history_len (2 * N + N)
int history[history_len] = empty
int history_index

#define FA(v, x) atomic {v = x; x++}
#define SWAP(a, b, c) atomic {a = b; b = c}

#define commit(v) history[history_index] = v -> history_index++

byte finished
active [n_actors] proctype enqueuer() {
  byte i
  int value

  /* starts with 2 to avoid -0 and -1 on dequeue */
  value = 2 + _pid

  d_step {
    FA(i, tail)
    commit(value)
  }

  /* not full */
  i + 1 != head

  i = i % N

  d_step {
    elements[i] = value
    commit(value)
    finished++
  }
}

active [n_actors] proctype dequeuer() {
  byte i
  byte tmp_index
  int v

start:
  i = head
  /* check empty */
  if
  :: i == tail -> goto start
  :: else
  fi

  i = i % N

  v = empty
  SWAP(v, elements[i], empty) ->
  /* mimic crash */
  if
  :: _pid == n_actors -> goto exit
  :: else
  fi

  if
  :: v == empty -> goto start
  :: else -> head++
  fi
exit:
  skip
  d_step {
    commit(-v)
    finished++
  }
}

init {
  /* at least two slot for the array, for we use one extra slot to distinguish
   * empty from full */
  assert(N > 1)
}
