## 1


### 1.A

If `count > 50` becomes true, there are totally 51 `load flag` in reader thread, and there are 1 `stroe flag` in writer thread. There are 52
combinations, so there are 52 inequivalent executions.

### 1.B

According to 'nidhuggc -sc main.c', there are 52 traces are explored.


### 1.C

`spin -a spin_main.pml && clang pan.c -DNOREDUCE -o pan && ./pan` to turn off partial order reduction, showing:

```
      673 states, stored
      310 states, matched
```

`spin -run` uses partial order reduction, and it reports the following. The number of states explored is reduced.

```
      569 states, stored
      102 states, matched
```

### 1.D

Change `count > 50` to `count > 1`, because `count` is just a local variable, whose value doesn't affect the global transition. Both spin and nidhugg
will explore less states in this configuration.

## 2

### 2.A

With POR, 12306 states are stored, while 73203 states are stored without it.

### 2.B

Introducing `ch_0` and `ch_1` for `one` and `two` message type, respectively and changing `CHANSIZE` from 4 to 2, to keep the total buffer size the
same. These changes bring #states to 4989. In addition, if `xs` and `xr` are used, spin could use them for more efficient searching, and #states is
further reduced to 1077.

### 2.C

Since addition on `local_sum` is commutative, and two senders use different channel, we could enlarge the channel size so that neither will be
blocked. Then, we could place them inside a `d_step` block, respectively, and this way, spin will only explore a single trace.
