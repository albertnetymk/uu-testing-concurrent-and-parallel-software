#include <stdio.h>
#include <assert.h>
#include <pthread.h>

volatile int x, flag;

void writer(void)
{
    x = 42;
    flag = 1;
}
void *reader(void *arg)
{
    int local = 0;
    int count = 0;
    local = flag;
    while (local != 1) {
        count++;
        if (count > 50) return NULL;
        local = flag;
    }
    assert(x == 42);
    return NULL;
}

int main() {
    pthread_t th_read;
    assert(pthread_create(&th_read, NULL, reader, NULL) == 0);
    writer();
}
