#define CHANSIZE 10
#define MAX   10
mtype       = { one, two, stop }
int  sum = 0
chan ch_0     = [CHANSIZE] of {mtype}
chan ch_1     = [CHANSIZE] of {mtype}

#define use_xsr

active proctype Sender1() {
#ifdef use_xsr
  xs ch_0
#endif
  d_step{
    int i = 0
    do
    :: i < MAX -> ch_0 ! one ; i++
    :: i >= MAX -> break
    od
    ch_0 ! stop
  }
}

active proctype Sender2() {
#ifdef use_xsr
  xs ch_1
#endif
  d_step{
    int i = 0
    do
    :: i < MAX -> ch_1 ! two ; i++
    :: i >= MAX -> break
    od
    ch_1 ! stop
  }
}
active proctype Receiver() {
#ifdef use_xsr
  xr ch_0
  xr ch_1
#endif
  int local_sum = 0
  int end_count = 0

  do
  ::  ch_0?one -> local_sum = local_sum + 1
  ::  ch_1?two -> local_sum = local_sum + 2
  ::  ch_0?stop ->
      if
      :: end_count == 0 -> end_count++
      :: end_count == 1 -> break
      fi
  :: ch_1?stop ->
      if
      :: end_count == 0 -> end_count++
      :: end_count == 1 -> break
      fi
  od
  sum = local_sum
}
