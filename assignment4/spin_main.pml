int x
bool flag

active proctype writer() {
  x = 42
  flag = true
}

active proctype read() {
  bool local_flag
  int count = 0
  local_flag = flag

  do
  :: !local_flag ->
        count++
        if
        :: count > 50 -> goto exit
        :: else -> local_flag = flag
        fi
  :: else -> break
  od

  assert(x == 42)
exit:
}
