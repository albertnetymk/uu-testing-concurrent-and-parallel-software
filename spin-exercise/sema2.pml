typedef semaphore {
  bool mutex;
  bool delay;
  int count;
  int wakecount;
}

int n_p;
int n_v;

bool ready

#define PB(x) d_step { x > 0 ; x--; }
#define VB(x) x = 1

semaphore s;

active [3] proctype actor_p() {
  ready;

  PB(s.mutex);
  s.count = s.count - 1;
  if
  :: s.count < 0 ->
    VB(s.mutex);
    PB(s.delay);
    PB(s.mutex);
    s.wakecount--;
    if
    :: s.wakecount > 0 -> VB(s.delay);
    :: else
    fi
  :: else
  fi
  VB(s.mutex);

  n_p++;

  /* should fail due to the algo being incorrect */
  assert(n_p <= n_v);
}

active [3] proctype actor_v() {
  ready;

  PB(s.mutex);
  s.count = s.count + 1;
  n_v++;
  if
  :: s.count <= 0 -> s.wakecount++; VB(s.delay)
  :: else
  fi

  VB(s.mutex);
}

init {
  d_step{
    s.mutex = 1;
    s.delay = 0;
    s.count = 0;
    s.wakecount = 0;

    ready = true;
  }
}
