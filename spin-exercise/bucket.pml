byte small, big

byte small_size = 3
byte big_size = 5

init {
loop:
  if
  :: big == 4 ->
      /* should fail, we found one solution */
      assert(false)
  :: else
  fi

  if
  :: small = 0
  :: big = 0
  :: small = small_size
  :: big = big_size
  :: small + big > big_size -> small = (small+big)-big_size; big = big_size
  :: small + big > small_size ->  big = (small+big)-small_size; small = small_size
  :: small + big <= big_size -> big = small + big ; small = 0
  :: small + big <= small_size -> small = small + big ; big = 0
  fi
  assert(big <= big_size && small <= small_size)
  goto loop
}
