int person[4] = {5, 10, 20, 25}
bool crossed[4]
bool torch_crossed
int time

init {
  int i,j
  bool all_crossed
start:

  all_crossed = crossed[0] && crossed[1] && crossed[2] && crossed[3]
  /* should fail, we can cross the bridge using 60 minutes */
  assert(!all_crossed)

  if
  :: crossed[0] == torch_crossed -> i = 0
  :: crossed[1] == torch_crossed -> i = 1
  :: crossed[2] == torch_crossed -> i = 2
  :: crossed[3] == torch_crossed -> i = 3
  fi

  if
  :: crossed[0] == torch_crossed -> j = 0
  :: crossed[1] == torch_crossed -> j = 1
  :: crossed[2] == torch_crossed -> j = 2
  :: crossed[3] == torch_crossed -> j = 3
  fi

  crossed[i] = ! torch_crossed
  crossed[j] = ! torch_crossed
  torch_crossed = ! torch_crossed

  time = time + (person[i] > person[j] -> person[i] : person[j])

  if
  :: time <= 60 -> goto start
  :: else
  fi
}
