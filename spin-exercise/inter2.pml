int x = 1

active [2] proctype actor() {
  int t1, t2

again:
  t1 = x
  t2 = x
  x = t1 + t2

  /* should fail, I think x is not bounded */
  assert(x < 10000000)
  goto again
}
