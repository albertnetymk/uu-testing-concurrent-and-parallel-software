int x, y, z

int enter

init {
  skip
}

active [2] proctype actor() {
  /* x y z is intialized to be zero */
  assert(_pid != 0)

again:
  x = _pid

  if
  :: y != 0 && y != _pid -> goto again
  :: else                -> skip
  fi

  z = _pid

  if
  :: x != _pid -> goto again
  :: else      -> skip
  fi

  y = _pid

  if
  :: z != _pid -> goto again
  :: else      -> skip
  fi

  enter++

  /* should fail because this lock is problematic */
  assert(enter <= 1)

  enter--

  x = 0
  y = 0
  z = 0

  goto again
}
