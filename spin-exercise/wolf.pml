bool farmer = 0
bool wolf = 0
bool sheep = 0
bool cabbage = 0

bool all_crossed

init {
start:
  all_crossed = farmer && wolf && sheep && cabbage
  /* should fail */
  assert(! all_crossed)

  if
  :: farmer == wolf -> wolf = ! farmer
  :: farmer == sheep -> sheep = ! farmer
  :: farmer == cabbage -> cabbage = ! farmer
  :: skip
  fi

  farmer = ! farmer

  if
  :: wolf == sheep && wolf != farmer -> goto exit
  :: else
  fi

  if
  :: sheep == cabbage && sheep != farmer -> goto exit
  :: else
  fi

  goto start
exit:
}
