#define N 5
chan channel[N] = [1] of { byte }

int candidates[N] = -1
byte done

active [N] proctype actor() {

  byte prev;
  byte x;
  if
  :: _pid == 0 -> prev = N - 1
  :: else -> prev = _pid - 1
  fi

  if
  :: goto initiator
  :: goto forwarder
  fi

initiator:
  channel[_pid] ! _pid
  byte min = _pid
  loop:
    channel[prev] ? x
    if
    :: x < min -> min = x
    :: x == _pid ->
      candidates[_pid] = min
      goto exit
    :: else -> skip
    fi
    channel[_pid] ! x
    goto loop

forwarder:
  do
  :: channel[prev] ? x ; channel[_pid] ! x
  :: timeout -> goto exit
  od

exit:
  done++
}

active proctype checker() {
  done == N
  d_step {
    byte leader
    int i
    /* check single leader */
    for (i in candidates) {
      if
      :: candidates[i] != -1 && leader == 0 -> leader = candidates[i]
      :: candidates[i] != -1 && leader != 0 -> assert(leader == candidates[i])
      :: else
      fi
    }
  }
}
