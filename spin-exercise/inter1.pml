int N
int done
active [2] proctype actor() {
  int i, temp

  for (i : 1 .. 10) {
    temp = N
    temp++
    N = temp
  }

  done++
}

active proctype check() {
  done == 2
  assert(N >= 2)
  /* should fail, for the minimal int is 2 */
  assert(N > 2)
}
