typedef semaphore {
  bool mutex;
  bool delay;
  int count;
}

bool ready

#define PB(x) d_step { x == 1 ; x = 0; }
#define VB(x) x = 1

semaphore s;

active [2] proctype actor_p() {
  ready;

  PB(s.mutex);
  s.count = s.count - 1;
  if
  :: s.count < 0 ->
    VB(s.mutex); PB(s.delay);
  :: else ->
    VB(s.mutex);
  fi
}

active [2] proctype actor_v() {
  ready;

  PB(s.mutex);
  s.count = s.count + 1;
  if
  :: s.count <= 0 -> VB(s.delay)
  :: else
  fi

  VB(s.mutex);
}


init {
  d_step {
    s.mutex = 1;
    s.delay = 0;
    s.count = 0;

    ready = true;
  }

  /* deadlock shouldn't happen, but due to error in this locking algo, it occurs */
}
