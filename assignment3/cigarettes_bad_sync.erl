-module(cigarettes_bad_sync).

-export([test/0]).

-include("little_helpers.hrl").

test() ->
  ?run(fun initialize/0, agent() ++ smokers()).

initialize() ->
  ?semaphore(agentSem, 1),
  ?semaphore(tobacco, 0),
  ?semaphore(paper, 0),
  ?semaphore(match, 0),
  ?write(tobacco_avail, false),
  ?write(paper_avail, false),
  ?write(match_avail, false).

agent() ->
  [fun tp/0, fun mt/0, fun pm/0].

tp() ->
  ?wait(agentSem),
  ?write(tobacco_avail, true),
  ?signal(tobacco),
  ?write(paper_avail, true),
  ?signal(paper).

mt() ->
  ?wait(agentSem),
  ?write(match_avail, true),
  ?signal(match),
  ?write(tobacco_avail, true),
  ?signal(tobacco).

pm() ->
  ?wait(agentSem),
  ?write(paper_avail, true),
  ?signal(paper),
  ?write(match_avail, true),
  ?signal(match).  

smokers() ->
  [fun m/0, fun p/0, fun t/0].

m() ->
  ?wait(tobacco),
  ?assert(?read(tobacco_avail)),
  ?write(tobacco_avail, false),
  ?print(m_got_tobacco),
  ?wait(paper),
  ?assert(?read(paper_avail)),
  ?write(paper_avail, false),
  ?print(m_got_paper),
  ?signal(agentSem).

p() ->
  ?wait(match),
  ?assert(?read(match_avail)),
  ?write(match_avail, false),
  ?print(p_got_match),
  ?wait(tobacco),
  ?assert(?read(tobacco_avail)),
  ?write(tobacco_avail, false),
  ?print(p_got_tobacco),
  ?signal(agentSem).

t() ->
  ?wait(paper),
  ?assert(?read(paper_avail)),
  ?write(paper_avail, false),
  ?print(t_got_paper),
  ?wait(match),
  ?assert(?read(match_avail)),
  ?write(match_avail, false),
  ?print(t_got_match),
  ?signal(agentSem).
