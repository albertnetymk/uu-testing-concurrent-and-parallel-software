-define(helper,little_helpers).

%%% Debug Macros

-define(print(A), erlang:display(A)).
-define(assert(A),
        case A of true -> ok;
          false ->
            ?print({assertion_failed, ?LINE}),
            throw({assertion_failed, ?LINE})
        end).

%%% General Macros

-define(run(A,B), ?helper:run(A,B)).
-define(run(A,B,C), ?helper:run(A,B,C)).
-define(run(A,B,C,D), ?helper:run(A,B,C,D)).

%%% Shared Variable Macros

-define(read(X), ?helper:read(X)).
-define(write(X,V), ?helper:write(X,V)).

%%% Semaphore Macros

-define(semaphore(S,I), ?helper:semaphore(S,I)).
-define(signal(S), ?helper:signal(S)).
-define(wait(S), ?helper:wait(S)).
