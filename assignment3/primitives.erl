-module(primitives).

-export([test_1/0, test_2/0, test_3/0]).

-include("little_helpers.hrl").

-define(workers, 2).

%%%=============================================================================

test(Init, Worker, Barrier, Check) ->
  ?run(Init, [Worker || _ <- lists:seq(1,?workers)], Barrier, Check).

check() ->
  ?workers =:= ?read(x).

%%%=============================================================================

test_1() ->
  test(fun initialize_1/0, fun worker_1/0, chain, fun check/0).

initialize_1() -> ?write(x,0).

worker_1() ->
  X = ?read(x),
  ?write(x, X+1).

%%%=============================================================================

test_2() ->
  test(fun initialize_2/0, fun worker_2/0, chain, fun() -> true end).

initialize_2() -> ?semaphore(mutex, 1).

worker_2() ->
  ?wait(mutex),
  ?signal(mutex).

%%%=============================================================================

test_3() ->
  test(fun initialize_3/0, fun worker_3/0, chain, fun check/0).

initialize_3() ->
  ?write(x,0),
  ?semaphore(mutex, 1).

worker_3() ->
  ?wait(mutex),
  X = ?read(x),
  ?write(x, X+1),
  ?signal(mutex).
