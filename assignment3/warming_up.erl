-module(warming_up).

-export([test/0, test_1/0, test_2/0, test_3/0]).

-include("little_helpers.hrl").

test() ->
  test_1().

test_1() ->
  Init = Worker = fun() -> ok end,
  ?run(Init, [Worker, Worker]).

test_2() ->
  Init = Worker = fun() -> ok end,
  % ?run(Init, [Worker, Worker], chain).
  ?run(Init, [Worker, Worker], watcher_chain).

test_3() ->
  test_2(),
  block().

block() ->
  receive after infinity -> ok end.

main(_) ->
  % test_1(),
  test_2(),
  io:format("~p~n", [ok]).
