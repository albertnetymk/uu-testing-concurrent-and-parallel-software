## 1

### 1.1.1

As the red arrow in `g.svg` shows, there's a racy between the `finish_token` from two workers. Two orders are explored, hence two interleaving.

### 1.1.2

- simple: all workers report back to the parent process on exit, a star topology
- chain: workers together with parent form a ring; on finishing the construction of the ring, parent passes the token to previous process, and each
  worker does the same. In the end, the token arrives back at parent.
- watcher_chain: similar to `chain` except that a watcher process is created for each worker so that on exit, each worker would inform its
  corresponding watcher to forward the finish token

### 1.1.3

Using `test_2` with each barrier method:

```
$ concuerror -m warming_up -t test_2 --graph g.dot --show_races ;  dot -Tsvg -o g.svg g.dot

simple: 2
chain: 1
watcher_chain: 4
```

### 1.1.4

- simple: pro: simple to set up and easy to understand; con: parent may be flooded with too many messages if #workers are large
- chain: pro: no single process receiving too many messages; con: worker processes may be kept alive for passing the token only
- watcher_chain: pro: worker processes exist after the work is done; con: extra number of actors are created

## 2

### 2.1.1

Using public table from ETS.

### 2.1.2

The condition `?workers =:= ?read(x)` doesn't hold.

### 2.1.3

3, for there are two racing instructions indicated in the plot.

### 2.1.4

Abstracting the problem to two processes with two statements in each process:

```
R1   R2

W1   W2
```

Considering only the interaction, we will get four orders:

- W1 -> R2
- R2 -> W1
- W2 -> R1
- R1 -> W2

The last one is already included by `W1 -> R2`, because it becomes `R1 -> W1 -> R2 -> W2` after adding the program order. Therefore, we only need to
explore three interleaving.


### 2.2.1

For each semaphore `S`, there are two keys `{S, s}` and `{S, w}` associated with it to keep track of the number of `signal` and `wait` have been
called. For `wait`, the caller's PID is recorded as well so that if it's blocked, the blocked process could be waken up. On calling `signal` or
`wait`, the corresponding key is queried for the current counter, N, and a new key `{S, N}` is inserted to `semaphore` table. `ets:insert_new` is used
so that we know if the same key exists in the table.

### 2.3.1

Access to `x` is protected by the semaphore, so it doesn't introduce any racing instructions, hence no more interleaving.

### 3.1.1

For each smoker `S`, the availability of the other two ingredients `S` is lacking is asserted.

### 3.1.2

`p()` and `t()` fails the assertion, leaving `mt()` and `pm()` blocking on the semaphore.

### 3.1.3

Yes, there are correct runs. `tp -> m -> mt -> p -> pm -> t` is a correct run.

### 3.1.4

The default one, `simeple`, takes quite long time; I killed it after a few minutes. `chain` finished quite quickly.

### 3.2.1

Each ingredient is model as a semaphore. The agent will choose one operation from `[tp, mt, pm]`, making the corresponding two ingredients
available. Each smoker will wait for the ingredients he/she's lacking; if successfully, it terminates normally.

### 3.2.2

`m` has acquired `tobacco` semaphore, and `t` has acquired `paper`. The system gets into a deadlock.

### 3.2.3

After using `--ignore_error deadlock`, `concuerror` doesn't report any error, so all error falls in the category of deadlock.

### 3.3.1

Let A, B, C be the three kind of ingredients, and a smoker requires A and B to smoke. Choose the value for them so that A -> B, B -> C, C -> A
coincide with the order the ingredients are distribute by the agent. Then, the smoker would perform the following steps:

```
  lock B

  if A is available
    // nothing
  else
    unlock B
    lock A
    unlock A
    lock B
    lock A
  end
```

### 3.3.2


Reusing the definitions from previous answer, and focusing the interaction between two threads, we can observe there are only two categories.

1. The second lock maybe interfered by another thread. In the following, T1 first acquires B, and when it tries to acquire A, A may be acquired by T2
   already.

```
  T1  T2
  A   C
  B   A
```

2. The second lock is not interfered. In the following, T1 first acquires B, and its acquiring of A is surely to success.

```
  T1  T2
  A   B
  B   C
```
### 3.4.1

Spin is more like a tool for validating an algorithm; concuerror may do so as well, but the semaphore example shows quite much basic
operation/structures need to be defined firstly. In other words, concuerror works more in a lower level. Concuerror could be used to test real Erlang
code, but Spin don't work with real application code.

### 4.1.1

On viewing the trace in svg file, different trace starts from another column, but following the arrows gives me the impression that the state on
its left visited as well, but according to the numbering, it's not part of that trace.

```
  1
  |
  2 - 2
```

Something like the following seems more logical to me:

```
  1 - 2
  |
  2
```

### 4.1.2

None
