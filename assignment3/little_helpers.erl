-module(little_helpers).

-export([run/2, run/3, run/4]).
-export([read/1, write/2]).
-export([semaphore/2, signal/1, wait/1]).

%%%=============================================================================

-type task()            :: fun(() -> ok).
-type workers_barrier() :: 'simple' | 'chain' | 'watcher_chain'.
-type condition()       :: fun(() -> boolean()).

%%%=============================================================================

-type semaphore()       :: atom().
-type variable()        :: atom().

%%%=============================================================================

-define(
   test_or_fail(Bool, Message, Data, Error),
   fun() ->
       case Bool of
         true -> ok;
         false ->
           stderr(Message, Data),
           error(Error)
       end
   end()).

-define(
   run_safe(Name, Fun, Error),
   fun() ->
       try
         Fun()
       catch
         error:Reason ->
           stderr(
             "~s crashed:~n"
             " Error: ~p~n"
             " Stacktrace:~n"
             "   ~p~n", [Name, Reason, erlang:get_stacktrace()]),
           error(Error)
       end
   end()).

%%%=============================================================================

-spec run(task(),[task()]) -> ok.

run(Initialize, Workers) ->
  run(Initialize, Workers, simple).

-spec run(task(), [task()], workers_barrier()) -> ok.

run(Initialize, Workers, WorkersBarrier) ->
  run(Initialize, Workers, WorkersBarrier, fun() -> true end).

-spec run(task(), [task()], workers_barrier(), condition()) -> ok.

run(Initialize, Workers, WorkersBarrier, Condition) ->
  Fun = fun() -> run_back(Initialize, Workers, WorkersBarrier, Condition) end,
  {Pid, Ref} = spawn_monitor(Fun),
  receive
    {'DOWN', Ref, process, Pid, Reason} -> Reason
  end.

run_back(Initialize, Workers, WorkersBarrier, Condition) ->
  ets:new(shared_variables, [named_table, public]),
  ets:new(semaphores, [named_table, public]),
  put(init, true),
  ?run_safe("The initialization function", Initialize, initialization_failed),
  erase(init),
  run_workers(Workers, WorkersBarrier),
  ?test_or_fail(
     true =:= ?run_safe("The condition function", Condition, condition_failed),
     "The condition function did not return true.",
     [],
     condition_not_true).

run_workers(Workers, WorkersBarrier) ->
  Parent = self(),
  FinishToken = finish_token,
  run_workers(Workers, 0, FinishToken, Parent, WorkersBarrier).

run_workers([], N, Token, Prev, WorkersBarrier) ->
  case WorkersBarrier =:= simple of
    true ->
      lists:foreach(fun(_) -> receive Token -> ok end end, lists:seq(1,N));
    false ->
      Prev ! Token,
      receive
        Token -> ok
      end
  end;
run_workers([Fun|Rest], N, Token, Prev, WorkersBarrier) ->
  Parent = self(),
  New =
    case WorkersBarrier of
      simple ->
        spawn(
          fun() ->
              ?run_safe(io_lib:format("Worker number ~p", [N]), Fun, worker_failed),
              Parent ! Token
          end);
      chain ->
        spawn(
          fun() ->
              ?run_safe(io_lib:format("Worker number ~p", [N]), Fun, worker_failed),
              receive Token -> Prev ! Token end
          end);
      watcher_chain ->
        spawn(
          fun() ->
              Pid =
                spawn(
                  fun() ->
                      receive
                        done ->
                          receive Token -> Prev ! Token end
                      end
                  end),
              Parent ! Pid,
              ?run_safe(io_lib:format("Worker number ~p", [N]), Fun, worker_failed),
              Pid ! done
          end),
        receive P -> P end
    end,
  run_workers(Rest, N + 1, Token, New, WorkersBarrier).

%%%=============================================================================

-spec read(variable()) -> term().

read(X) ->
  shared_variable({read, X}).

-spec write(variable(), term()) -> ok.

write(X, V) ->
  shared_variable({write, X, V}).

shared_variable(Op) ->
  try
    case Op of
      {read, X} ->
        case ets:lookup(shared_variables, X) of
          [{X, V}] -> V;
          [] ->
            stderr("Variable ~p was read before it was initialized.", [X]),
            error(value_not_initialized)
        end;
      {write, X, V} ->
        ets:insert(shared_variables, {X,V})
    end
  catch
    error:badarg ->
      stderr("Use ~p:run/2 to run your program.", [?MODULE]),
      error(variables_not_initialized)
  end.

%%%=============================================================================

-spec semaphore(semaphore(), integer()) -> semaphore().

semaphore(Name, N) ->
  ?test_or_fail(
     get(init) =:= true,
     "Semaphores should only be declared in the initialization function",
     [],
     semaphore_outside_init),
  ?test_or_fail(
     true =:= ets:insert_new(semaphores, [{{Name, E}, 0} || E <- [s,w]]),
     "A semaphore with name ~p already exists.",
     [Name],
     duplicate_semaphore),
  lists:foreach(fun(_) -> signal(Name) end, lists:seq(1,N)).

-spec signal(semaphore()) -> ok.

signal(S) ->
  Tail = ets:update_counter(semaphores, {S,s}, 1),
  case ets:insert_new(semaphores, {{S,Tail}, avoid}) of
    true -> ok;
    false ->
      P = read_s({S,Tail}),
      P ! continue
  end.

-spec wait(semaphore()) -> ok.

wait(S) ->
  Head = ets:update_counter(semaphores, {S,w}, 1),
  case ets:insert_new(semaphores, {{S,Head}, self()}) of
    true -> receive continue -> ok end;
    false -> ok
  end.

read_s(X) ->
  [{X,V}] = ets:lookup(semaphores, X),
  V.

%%%=============================================================================

stderr(Format, Data) ->
  Msg = lists:flatten(io_lib:format(Format, Data)),
  erlang:display_string(Msg).
