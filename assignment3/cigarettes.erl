-module(cigarettes).

-export([test/0]).
-export([test12/0, test13/0]).

-include("little_helpers.hrl").

test() ->
  ?run(fun initialize/0, agent() ++ smokers()).

test12() ->
  ?run(fun initialize/0, [fun tp/0, fun mt/0] ++ [fun m/0, fun p/0]).

test13() ->
  ?run(fun initialize/0, [fun tp/0, fun pm/0] ++ [fun m/0, fun t/0]).

initialize() ->
  ?semaphore(agentSem, 1),
  ?semaphore(tobacco, 0),
  ?semaphore(paper, 0),
  ?semaphore(match, 0),
  ?write(tobacco_avail, false),
  ?write(paper_avail, false),
  ?write(match_avail, false).

agent() ->
  [fun tp/0, fun mt/0, fun pm/0].

tp() ->
  ?wait(agentSem),
  ?write(tobacco_avail, true),
  ?signal(tobacco),
  ?write(paper_avail, true),
  ?signal(paper).

mt() ->
  ?wait(agentSem),
  ?write(match_avail, true),
  ?signal(match),
  ?write(tobacco_avail, true),
  ?signal(tobacco).

pm() ->
  ?wait(agentSem),
  ?write(paper_avail, true),
  ?signal(paper),
  ?write(match_avail, true),
  ?signal(match).

smokers() ->
  [fun m/0, fun p/0, fun t/0].

m() ->
  ?wait(paper),
  ?assert(?read(paper_avail)),

  case ?read(tobacco_avail) of
    true ->
      ?print(m_got_paper),
      ok;
    false ->
      ?signal(paper),
      ?wait(tobacco),
      ?signal(tobacco),

      ?wait(paper),
      ?assert(?read(paper_avail)),
      ?print(m_got_paper)
  end,

  ?wait(tobacco),
  ?assert(?read(tobacco_avail)),
  ?print(m_got_tobacco),

  ?write(tobacco_avail, false),
  ?write(paper_avail, false),

  ?signal(agentSem).

p() ->
  ?wait(tobacco),
  ?assert(?read(tobacco_avail)),

  case ?read(match_avail) of
    true ->
      ?print(p_got_tobacco),
      ok;
    false ->
      ?signal(tobacco),
      ?wait(match),
      ?signal(match),

      ?wait(tobacco),
      ?assert(?read(tobacco_avail)),
      ?print(p_got_tobacco)
  end,

  ?wait(match),
  ?assert(?read(match_avail)),
  ?print(p_got_match),

  ?write(tobacco_avail, false),
  ?write(match_avail, false),

  ?signal(agentSem).

t() ->
  ?wait(match),
  ?assert(?read(match_avail)),

  case ?read(paper_avail) of
    true ->
      ?print(t_got_match),
      ok;
    false ->
      ?signal(match),

      ?wait(paper),
      ?signal(paper),

      ?wait(match),
      ?assert(?read(match_avail)),
      ?print(t_got_match)
  end,

  ?wait(paper),
  ?assert(?read(paper_avail)),
  ?print(t_got_paper),

  ?write(paper_avail, false),
  ?write(match_avail, false),

  ?signal(agentSem).
