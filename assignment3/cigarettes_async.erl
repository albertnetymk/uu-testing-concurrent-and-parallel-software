-module(cigarettes_async).

-export([test/0]).

-include("little_helpers.hrl").

test() ->
  % ?run(fun initialize/0, agent() ++ smokers()).
  ?run(fun initialize/0, agent() ++ smokers(), chain).

initialize() ->
  ?semaphore(agentSem, 1),
  ?write(tobacco_avail, false),
  ?write(paper_avail, false),
  ?write(match_avail, false).

agent() ->
  [fun tp/0, fun mt/0, fun pm/0].

tp() ->
  ?wait(agentSem),
  ?write(tobacco_avail, true),
  ?write(paper_avail, true).

mt() ->
  ?wait(agentSem),
  ?write(match_avail, true),
  ?write(tobacco_avail, true).

pm() ->
  ?wait(agentSem),
  ?write(paper_avail, true),
  ?write(match_avail, true).

smokers() ->
  [fun m/0, fun p/0, fun t/0].

m() ->
  ?assert(?read(tobacco_avail)),
  ?write(tobacco_avail, false),
  ?print(m_got_tobacco),
  ?assert(?read(paper_avail)),
  ?write(paper_avail, false),
  ?print(m_got_paper),
  ?print(m_made_cigarette),
  ?signal(agentSem).

p() ->
  ?assert(?read(match_avail)),
  ?write(match_avail, false),
  ?print(p_got_match),
  ?assert(?read(tobacco_avail)),
  ?write(tobacco_avail, false),
  ?print(p_got_tobacco),
  ?print(p_made_cigarette),
  ?signal(agentSem).

t() ->
  ?assert(?read(paper_avail)),
  ?write(paper_avail, false),
  ?print(t_got_paper),
  ?assert(?read(match_avail)),
  ?write(match_avail, false),
  ?print(t_got_match),
  ?signal(agentSem).
