## Theory

### Enhancing symbolic execution with veritesting

#### 1. Read.

Done

#### 2. Summary

The paper starts with the background of symbolic execution technique, which consists of two major categories, dynamic symbolic execution (DSE) and
static symbolic execution (SSE). The former maintains an execution state, a triplet (pc, phi, delta), covering program counter indicating location of
the current executing statement in the program, path predicate allowing the current exploring path to be taken, and environment for values of all
variables. In order to achieve complete path coverage, execution state needs to be forked on each branch, resulting into path explosion in certain
cases. The second approach tries to summarize the effect of branches on merging point, which avoids the path explosion problem aforementioned.
However, in the presence of system calls, this approach falls short.

Starting in section 3, the paper uses an example to motivate the veritesting scheme, that combines the strength of DSE and SSE. On branching,
veritesting obtains a partial CFG, ending at function boundaries, system calls or and unknown instructions, from the branching point. In CFG, loop
unrolling happens based on info from DSE. Artificial node "Incomplete loop" is constructed to accommodate potential execution after exploring the
unrolling loop. In addition, transition points are identified so that DSE will be continued.

SSE will be applied on the acyclic CFG, and the final node, merging all incoming edges, will be of form using ite (if-then-else). Because of this
merging operator, path explosion is avoided.

#### 3. Interesting

All examples used in section 3 are C-like code snippets, so I assumed that the tool works on source code. However, according to the evaluation
section, it works on binary directly.

The idea of merging multiple paths into one is insightful and apparently it could mitigate the path explosion to some extent.

#### 4. Comments

Since it works on the binary level, maybe buggy code is transformed/removed by the compiler/optimizer so that the program never crashes even if full
path coverage is achieved.


### Concolic Testing for Functional Languages

#### 1. Read.

Done

#### 2. Summary

This paper demonstrates concolic testing for functional languages via a tool targeting programs written in Erlang. The tool works on AST of Core
Erlang nodes, and annotates case-clause ones with branching info. The AST is interpreted, and its trace is recorded along the concrete and symbolic
values on branches (up to a certain depth). The result of execution is recorded on termination, and new inputs are generated via SMT solver by
negating one of the branching predicates. The current implementation prefers predicates closer to the root, so basically breadth-first search is
conducted.

#### 3. Interesting

Carefully constructing queue item as a tuple, so that no sophisticated comparator needs to be used in minheap.

It could use type specification to synthesize values of interesting type.

#### 4. Comments

The paper contains sufficient amount of detail to follow without too much prior knowledge in this domain, and it's crucial to get a high level
overview of the algorithm and the general architecture before diving into the code directly.

## Practice

The item in `TagQueue` has the format of `{visited, operation_id, tag_id, handle}`, and `<` ordering is used on picking the smallest item in the
original implementation. The new commit changes the search so that `last_clause` is preferred.

The commit (on top of `improve_search` branch) consists of two major parts: 1. `get_useful_info_from_ann` traverses AST to collect `tag_id` that lead
to `last_clause` into a set and 2. `get_useful_info` utilizes the info gather in previous step so that when tags existing in the set are preferred.

Simple prints (which tag is popped from the `TagQueue`, and what solution SMT solver returns) are added to the code so that we know the changes above
is effective. Only changed file are included in `src`, namely:

```
src/cuter_cerl.erl
src/cuter_minheap.erl
src/cuter_scheduler_maxcover.erl
src/cuter_solver.erl
```

Using `hello_world.erl` as an example:

```erlang
-module(hello_world).
-mode(compile).

-compile(export_all).

add(2) -> ok;
add(1) -> ok;
add(0) -> ok.
```

The original implementation with prints:
```
$ cuter hello_world add '[0]' -r
Compiling hello_world.erl ... OK
Testing hello_world:add/1 ...
.2
{ok,[2]}
6
.{ok,[1]}
9
.{ok,[3]}

hello_world:add(3)

=== Inputs That Lead to Runtime Errors ===
#1 hello_world:add(3)
```

Applying the aforementioned change; here we can see erronous input `3` is found earlier.

```
$ cuter hello_world add '[0]' -r
Compiling hello_world.erl ... OK
Testing hello_world:add/1 ...
.9
{ok,[3]}
2

hello_world:add(3)
{ok,[2]}
6
.{ok,[1]}
.
=== Inputs That Lead to Runtime Errors ===
#1 hello_world:add(3)
```
